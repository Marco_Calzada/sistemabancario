package com.ks.bancario.accounts.credits;

import com.ks.bancario.accounts.TipoTarjeta;

/**
 * Created by migue on 16/11/2016.
 */
public class Clasic extends Credit implements TipoTarjeta {
    private static String type;
    public Clasic()
    {
        balance = 10000;
        interes = 10;
        limit = 10000;

    }
    static {
        type = "Clasica";
    }
    @Override
    public String getType(){
        return type;
    }
}