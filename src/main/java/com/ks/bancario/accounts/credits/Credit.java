package com.ks.bancario.accounts.credits;

import com.ks.bancario.accounts.Account;
import com.ks.bancario.accounts.Contador;

/**
 * Created by migue on 16/11/2016.
 */
public class Credit extends Account implements Contador
{
    protected float interes;
    protected float limit;
    private static int contador;

    static {
        contador = 0;
    }

    public Credit()
    {
        contador += 1;
        interes = 5;
        limit = 5000;
        balance = 5000;
    }

    public float getInteres()
    {
        return interes;
    }

    public void setInteres(float interes)
    {
        this.interes = interes;
    }

    public float getLimit()
    {
        return limit;
    }

    public void setLimit(float limit)
    {
        this.limit = limit;
    }

    @Override
    public int getContador(){
        return contador;
    }
}