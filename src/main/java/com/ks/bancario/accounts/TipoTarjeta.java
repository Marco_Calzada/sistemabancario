package com.ks.bancario.accounts;

/**
 * Created by mcalzada on 28/11/16.
 */
public interface TipoTarjeta {
    public abstract String getType();
}
