package com.ks.bancario.accounts;


import java.util.Date;

/**
 * Created by migue on 16/11/2016.
 */
public class Account implements Contador, TipoTarjeta
{
    private String card;
    private String clabe;
    private static int contador;
    private static String type;
    protected float balance;
    private Date expiration;
    private String cvv;
    protected float annuity;

    static {
        contador = 0;
        type = null;
    }
    public Account()
    {
        balance = 0;
        contador += 1;
        card = "";
        clabe = "";
        annuity = (float) 500.00;
    }

    public String getCard()
    {
        return card;
    }

    public void setCard(String card)
    {
        this.card = card;
    }

    public String getClabe()
    {
        return clabe;
    }

    public void setClabe(String clabe)
    {
        this.clabe = clabe;
    }

    public float getBalance()
    {
        return balance;
    }

    public Date getExpiration()
    {
        return expiration;
    }

    public void setExpiration(Date expiration)
    {
        this.expiration = expiration;
    }

    public String getCvv()
    {
        return cvv;
    }

    public void setCvv(String cvv)
    {
        this.cvv = cvv;
    }

    public float getAnnuity()
    {
        return annuity;
    }

    public void addBalance(float amount)
    {
        balance += amount;
    }

    public boolean substractBalance(float amount)
    {
        if (balance >= amount)
        {
            balance -= amount;
            return true;
        }
        return false;
    }

    public int getContador() {
        return contador;
    }

    public String getType() {
        return type;
    }
}