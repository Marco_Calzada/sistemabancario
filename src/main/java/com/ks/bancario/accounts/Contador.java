package com.ks.bancario.accounts;

/**
 * Created by mcalzada on 25/11/16.
 */
public interface Contador {
    public abstract int getContador();
}
