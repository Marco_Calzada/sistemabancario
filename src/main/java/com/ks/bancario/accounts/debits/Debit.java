package com.ks.bancario.accounts.debits;

import com.ks.bancario.accounts.Account;
import com.ks.bancario.accounts.Contador;

/**
 * Created by migue on 16/11/2016.
 */
public class Debit extends Account implements Contador
{
    public static int contador;

    static {
        contador = 0;
    }
    public Debit()
    {
        contador += 1;
    }

    @Override
    public int getContador(){
        return contador;
    }
}
